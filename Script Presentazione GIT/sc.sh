#!/bin/bash
echo "original file1 content" > file1.txt
git add file1.txt
git commit -m "first commit on master"
git checkout -b feature1
echo "file2 content for feature1" > file2.txt
git add file2.txt
echo "added new content for feature1" >> file1.txt
cat file1.txt
git add file1.txt
git commit -m "applied modifications for feature1"
git checkout master
echo "added file3 for bug fix" > file3.txt
git add file3.txt
cat file1.txt
echo 'bug fixed directly on master!' >> file1.txt
cat file1.txt
git add file1.txt
git commit -m "bug fixed on master"
git merge feature1
