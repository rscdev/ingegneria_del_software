import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


class Main {
	public static void main (String argv[]) {
		List<int[]> a=Arrays.asList(new int[] { 2, 2, 3 },
new int[] { 3, 2, 3 }, new int[] { 3, 3, 3 }, new int[] { 3, 4, 5 },
new int[] { 5, 2, 3 });
		a.stream().filter (s -> Main.check(s)==true).map (s ->s[0]+s[1]+s[2]).forEach(s -> System.out.println (s+" - ")); 
	}
	
	public static boolean check (int []v) {
		return (v[0]<v[1]+v[2] && v[1]<v[0]+v[2] && v[2]<v[0]+v[1]);
	}
	
}
