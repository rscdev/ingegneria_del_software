import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


class Main {
	public static void main (String argv[]) {
		List<String> a=Arrays.asList("author","auto","autocorrect","begin","big","bigger","biggish");
		a.stream().filter(s-> s.substring(0,2).equals ("au")).forEach(s->System.out.print (s+" - "));
	}
}
